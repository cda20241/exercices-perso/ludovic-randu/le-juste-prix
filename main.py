import random
import time

MINI = 1
MAXI =1000

def menu():
    """
    Fonction qui affiche le menu principal du jeu
    :return: Renvoie le choix du joueur
    """
    print("#############################################################|")
    print("|                     MENU PRINCIPAL                         | ")
    print("|############################################################|")
    print("")
    print("Affichage des derniers scores")
    print("")
    print("1) Scores")
    print("2) Jouer")
    choix = input("Choisir une option : ")
    if choix == '1':
        menu_scores()
    elif choix == '2':
        menu_partie()


def menu_scores():
    """
    Fonction les 10 derniers résultats de joueurs
    :return:
    """
    print("#############################################################|")
    print("|                          SCORES                            | ")
    print("|############################################################|")
    print("")
    fichier = open("scores.txt", "r")
    for ligne in fichier:
         print(ligne)
    fichier.close()
    print("")
    print("")
    print("Exit pour revenir au menu principal")
    choix = input("Entrée : ")
# convertir les majuscules en minuscules
    if choix == 'exit':
        menu()

def menu_partie():
    print("#############################################################|")
    print("|                      LE JUSTE PRIX                         | ")
    print("|############################################################|")
    print("")
    print("Veuillez choisir un nom de joueur:")
    joueur = input()
    chrono = time.time()
    # Vérifier si le nom d'utilisateur n'est pas exit et qu'il n'y ai pas le caractère ":"
    print('Bonjour '+joueur)
    chiffre = nombre(MINI, MAXI)
    partie(chiffre, joueur, chrono)



def partie(chiffre, joueur, chrono):
    print('Choisir un nombre entre ' + str(MINI) + ' et ' + str(MAXI)+ " :")
    reponse = input('')
    reponse = int(reponse)
    tentative = 1
    while reponse != chiffre:
        if reponse < chiffre and reponse >= MINI :
            print("C'est plus")
            print('Choisir un nouveau nombre entre ' + str(MINI) + ' et ' + str(MAXI) + " :")
            reponse=input('')
            reponse = int(reponse)
            tentative += 1
        elif reponse > chiffre and reponse < MAXI :
            print("C'est moins")
            print('Choisir un nouveau nombre entre ' + str(MINI) + ' et ' + str(MAXI) + " :")
            reponse = input('')
            reponse = int(reponse)
            tentative += 1
        else :
            print("Entrée incorrecte")
            break
    nouveau_chrono = time.time() - chrono
    print('Vous avez gagné '+joueur+'! Le chiffre était bien le ' + str(chiffre)+' en '+str(tentative)+' trentatives en '+str(nouveau_chrono)+' secondes' )
    fichier = open("scores.txt", "a")
    fichier.write("\n" +joueur+ " / " +str(tentative)+ " / " +str(nouveau_chrono))
    fichier.close()
    print('Tapez Entrée pour returner au menu principal:')
    retour=input('')
    if retour == '':
        menu()



def nombre(mini, maxi):
    """
    Fonction qui renvoi un nombre compris entre le chiffre mini et le chiffre maxi donnés
    :param mini: Chiffre minimum
    :param maxi: Chiffre maximum
    :return: Un chiffre compris entre le chiffre mini et maxi
    """
    return random.randint(int(mini), int(maxi))


if __name__ == '__main__':
    # Affichage du menu principal et récupération du choix du joueur
    choix = menu()

